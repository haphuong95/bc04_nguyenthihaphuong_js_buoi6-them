function ketQua() {
  var soN = document.getElementById("so-n").value * 1;
  var soNguyenTo = 0;
  for (var i = 2; i <= soN; i++) {
    // biến đếm là i
    if (i % 2 != 0 && i % Math.sqrt(i) != 0) {
      soNguyenTo = soNguyenTo + `-` + `${i}`;
    }
  }

  document.getElementById(
    "result"
  ).innerHTML = `<h3 class="py-4"> Số nguyên tố là: ${soNguyenTo}</h3>`;
}
